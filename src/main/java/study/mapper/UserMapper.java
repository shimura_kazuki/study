package study.mapper;

import java.util.List;

import study.dto.UserDto;
import study.entity.User;

public interface UserMapper {
	//user1人取得
    User getUser(int id);

    //全user取得
    List<User> getUsers();

    //登録する
    int insertUser(UserDto dto);

    //削除する
	int deleteUser(int id);

	//更新する
	int updateUser(UserDto dto);

}