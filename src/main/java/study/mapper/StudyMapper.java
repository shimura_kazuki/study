package study.mapper;

import java.util.List;

import study.entity.Study;

public interface StudyMapper {
	//全学習記録取得
	List<Study> getStudies();


}
