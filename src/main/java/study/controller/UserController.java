package study.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import study.dto.UserDto;
import study.form.UserForm;
import study.service.UserService;


@Controller
public class UserController {

	//	DI(依存性の注入) UserServiceで使っているメソッドを使えるようにして
	//	仮にuserServiceでメソッドを作っていなくでもエラーが出ないようにする
	@Autowired
	private UserService userService;

	//一件取得のメソッド
	@RequestMapping(value ="/user/{id}", method = RequestMethod.GET)
	public String user(Model model, @PathVariable int id) {
		UserDto user = userService.getUser(id);
		model.addAttribute("message", "MyBatisのサンプルです");
		model.addAttribute("user",  user);
		return "user";
	}

	//全件取得のメソッド
	@RequestMapping(value = "/user/", method = RequestMethod.GET)
	public String Users(Model model) {
		List<UserDto> users = userService.getUsers();
		model.addAttribute("message", "MyBatisの全件取得サンプルです");
		model.addAttribute("users", users);
		return "users";
	}

	//データの登録 (GETリクエスト)
	@RequestMapping(value = "/user/signup/", method = RequestMethod.GET)
	public String userInsert(Model model) {
	    UserForm form = new UserForm();
	    model.addAttribute("userForm", form);
	    model.addAttribute("message", "MyBatisのinsertサンプルです。");
	    return "userInsert";
	}
	//データの登録 (POSTリクエスト)
	@RequestMapping(value = "/user/signup/", method = RequestMethod.POST)
	public String userInsert(@ModelAttribute UserForm form, Model model) {
//	    List<UserDto> user = userService.insertUser(
//	    		form.getAccount(), form.getName(), form.getPassword());

	    UserDto dto = new UserDto();
		BeanUtils.copyProperties(form, dto);
		int count = userService.insertUser(dto);

//	    Logger.getLogger(TestController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
	    return "redirect:/user/";
	}

	//データの削除 (GETリクエスト)
	@RequestMapping(value = "/user/delete/", method = RequestMethod.GET)
	public String userDelete(Model model) {
		UserForm form = new UserForm();
		model.addAttribute("userForm", form);
		model.addAttribute("message", "MyBatisのdeleteサンプルです");
		return "userDelete";
	}
	//データの削除 (POSTリクエスト)
	@RequestMapping(value = "/user/delete/", method = RequestMethod.POST)
	public String userDelete(@ModelAttribute UserForm form, Model model) {
		int count = userService.deleteUser(form.getId());
//		Logger.getLogger(TestController.class).log(Level.INFO, "削除件数は" + count + "件です。");
		return "redirect:/user/";
	}

	//データの更新 (GETリクエスト)
	@RequestMapping(value = "/user/update/{id}", method = RequestMethod.GET)
	public String userUpdate(Model model, @PathVariable int id) {
		UserDto user = userService.getUser(id);
		model.addAttribute("message", "MyBatisのUpdateサンプルです");
		model.addAttribute("user", user);
		UserForm form = new UserForm();
		form.setId(user.getId());
		form.setAccount(user.getAccount());
		form.setName(user.getName());
		form.setPassword(user.getPassword());
		model.addAttribute("userForm", form);
		return "userUpdate";
	}

	// ユーザー更新 (POSTリクエスト)
	@RequestMapping(value = "/user/update/{id}/", method = RequestMethod.POST)
	public String userUpdate(Model model, @ModelAttribute UserForm form) {
		UserDto dto = new UserDto();
		BeanUtils.copyProperties(form, dto);
		int count = userService.updateUser(dto);
//		Logger.getLogger(TestController.class).log(Level.INFO, "更新件数は" + count + "件です。");
		return "redirect:/user/";
	}
}