<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
	<meta charset="utf-8">
	<title>Welcome</title>
</head>
<body>
	<header>
		<a href="${pageContext.request.contextPath}/user/">ユーザー一覧</a>
		<a href="${pageContext.request.contextPath}/user/insert/input/">新規ユーザー登録</a>
		<a href="${pageContext.request.contextPath}/user/delete/input/">ユーザー削除</a>
	</header>
	<div>
		<h1>トップページです。</h1>
		<p>index.jspを呼び出しています。</p>
	</div>
</body>
</html>
