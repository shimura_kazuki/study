<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー削除</title>
</head>
<body>
	<header>
		<a href="${pageContext.request.contextPath}/user/">ユーザー一覧</a>
		<a href="${pageContext.request.contextPath}/user/signup/">新規ユーザー登録</a>
		<a href="${pageContext.request.contextPath}/user/delete/">ユーザー削除</a>
	</header>
	<div>
		<h1>${message}</h1>
		<h2>削除するユーザーのIDを入力してください</h2>
		<form:form modelAttribute="userForm">
			ID:<form:input path="id" />
			<input type="submit">
		</form:form>
	</div>
</body>
</html>