<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー詳細</title>
</head>
<body>
	<header>
		<a href="${pageContext.request.contextPath}/user/">ユーザー一覧</a>
		<a href="${pageContext.request.contextPath}/user/signup/">新規ユーザー登録</a>
		<a href="${pageContext.request.contextPath}/user/delete/">ユーザー削除</a>
		<a href="${pageContext.request.contextPath}/studies/">学習記録一覧</a>
	</header>
	<div>
		<h1>${message}</h1>
		<p>id : ${user.id}</p>
		<p>account : ${user.account}</p>
		<p>name : ${user.name}</p>
		<p>password : ${user.password}</p>
		<p>created_at : ${user.created_at}</p>
		<p>updated_at : ${user.updated_at}</p>
	</div>

</body>
</html>