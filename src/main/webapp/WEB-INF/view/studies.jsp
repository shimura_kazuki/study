<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>学習記録一覧</title>
</head>
<body>
	<header>
		<a href="${pageContext.request.contextPath}/user/">ユーザー一覧</a>
		<a href="${pageContext.request.contextPath}/user/signup/">新規ユーザー登録</a>
		<a href="${pageContext.request.contextPath}/user/delete/">ユーザー削除</a>
		<a href="${pageContext.request.contextPath}/studies/">学習記録一覧</a>
	</header>
	<div>
		<h2>全ユーザーの学習一覧</h2>
		<c:forEach items="${studies}" var="study">
			<p>id : <c:out value="${study.id}"></c:out></p>
			<p>user_id : <c:out value="${ study.user_id }"></c:out></p>
			<p>title : <c:out value="${ study.title }"></c:out></p>
			<p>category : <c:out value="${ study.category_id }"></c:out></p>
			<p>text : <c:out value="${ study.text }"></c:out></p>
			<p>created_at : <c:out value="${ study.created_at }"></c:out></p>
			<p>updated_at : <c:out value="${ study.updated_at }"></c:out></p>
		</c:forEach>
	</div>

</body>
</html>