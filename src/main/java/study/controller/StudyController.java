package study.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import study.dto.StudyDto;
import study.service.StudyService;

@Controller
public class StudyController {

	//	DI(依存性の注入) StudyServiceで使っているメソッドを使えるようにして
	//	仮にuserServiceでメソッドを作っていなくでもエラーが出ないようにする
	@Autowired
	private StudyService studyService;

		//全件取得のメソッド
		@RequestMapping(value = "/studies/", method = RequestMethod.GET)
		public String Studies(Model model) {
			List<StudyDto> studies = studyService.getStudies();
			model.addAttribute("message", "MyBatisの全件取得サンプルです");
			model.addAttribute("studies", studies);
			return "studies";
		}
		
		
//		
//		
//  		if ( !StringUtils.isEmpty(dto.getPassword()) ) {
//            String encPassword = CipherUtil.encrypt( dto.getPassword() );
//            dto.setPassword(encPassword);
//        }
//  	//パスワード暗号化
//    	if ( !dto.getPassword().isEmpty() ) {
//            String encPassword = CipherUtil.encrypt(dto.getPassword());
//            dto.setPassword(encPassword);
//        }
//
//  		if ( dto.getPassword() != " " ) {
//            String encPassword = CipherUtil.encrypt(dto.getPassword());
//            dto.setPassword(encPassword);
//        }
//  		if ( dto.getPassword() != null ) {
//  			String encPassword = CipherUtil.encrypt(dto.getPassword());
//  			dto.setPassword(encPassword);
//  		}

}
