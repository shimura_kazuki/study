データベース作成
create database study;

データベース使用
use study;

------------------------------------------------------------------------------------------------------------------------
<テーブル作成>

usersテーブル
create table users (
  id integer primary key not null auto_increment,
  account varchar(20) not null unique,
  password varchar(255) not null,
  name varchar(10) not null,
  created_at timestamp not null default current_timestamp(),
  updated_at timestamp not null default current_timestamp()
);

usersテーブル(insert文)
insert into users (
  id,
  account,
  password,
  name,
  created_at,
  updated_at
) values (
  1,
  'first',
  'first',
  '一郎',
  current_timestamp(),
  current_timestamp()
);

-------------------------------------------------------------------------------------------------------------------

学習投稿テーブル
create table studies (
  id integer primary key not null auto_increment,
  user_id integer not null,
  title varchar(255) not null,
  category_id integer not null,
  text text not null,
  created_at timestamp not null default current_timestamp(),
  updated_at timestamp not null default current_timestamp()
);

学習投稿テーブル(insert文)
insert into studies (
  id,
  user_id,
  title,
  category_id,
  text,
  created_at,
  updated_at
) values (
  1,
  1,
  '一番目の投稿(user_id = 1)',
  1,
  '1番目の学習投稿の本文です。(user_id = 1)',
  current_timestamp(),
  current_timestamp()
);


select count(user_id), sum(drive_point), sum(analyze_point), sum(volunteer_point), sum(create_point) from style_results a where not exists ( select * from style_results b where a.user_id = b.user_id and a.created_date < b.created_date);