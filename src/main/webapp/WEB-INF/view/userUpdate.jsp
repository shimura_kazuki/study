<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー編集</title>
</head>
<body>
	<header>
		<a href="${pageContext.request.contextPath}/user/">ユーザー一覧</a>
		<a href="${pageContext.request.contextPath}/user/signup/">新規ユーザー登録</a>
		<a href="${pageContext.request.contextPath}/user/delete/">ユーザー削除</a>
	</header>
	<div>
		<h1>${message}</h1>
		<h2>ユーザー情報を更新してください</h2>
		<form:form modelAttribute="userForm">
			<p>ID : ${user.id}</p>
			<p>ACCOUNT : <form:input path="account" value="${ user.account }" /></p>
			<p>NAME : <form:input path="name" value="${ user.name }" /></p>
			<p>PASS : <form:input path="password" value="${ user.password }" /></p>
			<input type="submit">
		</form:form>
	</div>
</body>
</html>