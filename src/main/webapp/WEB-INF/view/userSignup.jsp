<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>新規ユーザー登録</title>
</head>
<body>
	<header>
		<a href="${pageContext.request.contextPath}/user/">ユーザー一覧</a>
		<a href="${pageContext.request.contextPath}/user/signup/">新規ユーザー登録</a>
		<a href="${pageContext.request.contextPath}/user/delete/">ユーザー削除</a>
	</header>
	<div>
		<h1>${ message }</h1>
		<h2>新規登録するユーザー情報を入力してください</h2>
		<form:form modelAttribute="userForm">
			<p>account : <form:input path="account" /></p>
			<p>name : <form:input path="name" /></p>
			<p>pass : <form:input path="password" /></p>
			<input type="submit">
		</form:form>
	</div>
</body>
</html>