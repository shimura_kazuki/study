package study.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import study.dto.StudyDto;
import study.entity.Study;
import study.mapper.StudyMapper;

@Service
public class StudyService {

	@Autowired
    private StudyMapper studyMapper;

	public List<StudyDto> getStudies() {
		//一つの学習記録情報をとってきている
		List<Study> studyList = studyMapper.getStudies();
		//取ってきた学習記録を全部取ってきた入れる
		List<StudyDto> resultList = convertToDto(studyList);
		return resultList;
	}


	private List<StudyDto> convertToDto(List<Study> studyList) {
		//LinkedList<>()は空のリストを構成するメソッドらしい
		List<StudyDto> resultList = new LinkedList<>();
		for (Study entity : studyList) {
			StudyDto dto = new StudyDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

}
