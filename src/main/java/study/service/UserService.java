package study.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import study.dto.UserDto;
import study.entity.User;
import study.mapper.UserMapper;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public UserDto getUser(Integer id) {
        UserDto dto = new UserDto();
        User entity = userMapper.getUser(id);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public List<UserDto> getUsers() {
        List<User> userList = userMapper.getUsers();
        List<UserDto> resultList = convertToDto(userList);
        return resultList;
    }

    private List<UserDto> convertToDto(List<User> userList) {
        List<UserDto> resultList = new LinkedList<>();
        for (User entity : userList) {
            UserDto dto = new UserDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

//    //登録する
//    public List<UserDto> insertUser(String account, String name, String password) {
//    	List<UserDto> user = userMapper.insertUser(account, name, password);
//    	return user;
//    }
    //登録する
    public int insertUser(UserDto dto) {
    	int count = userMapper.insertUser(dto);
		return count;
	}

    //削除する
	public int deleteUser(int id) {
		int count = userMapper.deleteUser(id);
		return count;
	}

	//更新する
	public int updateUser(UserDto dto) {
		int count = userMapper.updateUser(dto);
		return count;
	}

}