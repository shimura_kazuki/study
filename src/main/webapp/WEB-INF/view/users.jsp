<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<meta charset="utf-8">
	<title>ユーザー一覧</title>
</head>
<body>
	<header>
		<a href="${pageContext.request.contextPath}/user/">ユーザー一覧</a>
		<a href="${pageContext.request.contextPath}/user/signup/">新規ユーザー登録</a>
		<a href="${pageContext.request.contextPath}/user/delete/">ユーザー削除</a>
		<a href="${pageContext.request.contextPath}/studies/">学習記録一覧</a>
	</header>
	<div>
		<h1>${message}</h1>
		<c:forEach items="${users}" var="user">
			<p>id : <c:out value="${user.id}"></c:out></p>
			<p>account : <c:out value="${ user.account }"></c:out></p>
			<p>name : <c:out value="${ user.name }"></c:out></p>
			<p>password : <c:out value="${ user.password }"></c:out></p>
			<p>created_at : <c:out value="${ user.created_at }"></c:out></p>
			<p>updated_at : <c:out value="${ user.updated_at }"></c:out></p>
			<a href="${pageContext.request.contextPath}/user/update/${user.id}/">編集</a>
		</c:forEach>
	</div>
</body>
</html>